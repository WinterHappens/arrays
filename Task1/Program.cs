﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Вывести массив чисел от 1 до 20

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[20];

            for (int i = 1; i <= 20; i++)
            {
                array[i - 1] = i;
            }
            for(int i = 0; i < 20; i++)
            {
                Console.Write(array[i]+" ");
            }
            Console.ReadKey();
        }
    }
}
