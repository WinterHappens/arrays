﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// При вводе индекса ячейки массива, 
// программа должна выдавать значение в этой ячейке

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int Length = 20;

            int[] array = new int[Length];

            Random rand = new Random();
            for (int j = 0; j < Length; j++)
                array[j] = rand.Next(100);

            Console.WriteLine($"Введите индекс массива от 0 до {Length - 1}");
            int i = int.Parse(Console.ReadLine());

            for (int j = 0; j < Length; j++)
            {
                Console.Write(array[j]+" ");
            }
            Console.WriteLine();

            Console.WriteLine(array[i]);
            Console.ReadKey();
        }
    }
}
