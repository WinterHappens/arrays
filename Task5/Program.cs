﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Дан массив. Напечатать все неотрицательные элементы

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            int arrayLength;
            Console.Write("Введите длину массива: ");
            arrayLength = int.Parse(Console.ReadLine());

            int[] array = new int[arrayLength];

            Random rnd = new Random();

            for (int i = 0; i < arrayLength; i++)
            {
                array[i] = rnd.Next(-100,100);
            }
            Console.WriteLine("Массив целиком: ");
            for (int i = 0; i < arrayLength; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();

            Console.WriteLine("Только неотрицательные элементы массива: ");
            for (int i = 0; i < arrayLength; i++)
            {
                if (array[i] >= 0)
                {
                    Console.Write(array[i] + " ");
                }
            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
