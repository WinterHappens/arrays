﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Вывести элементы массива в обратном порядке

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int arrayLength; 
            Console.Write("Введите длину массива: ");
            arrayLength = int.Parse(Console.ReadLine());

            int[] array = new int[arrayLength];

            Random rnd = new Random();

            for(int i = 0; i < arrayLength; i++)
            {
                array[i] = rnd.Next(100);
            }

            Console.WriteLine("Массив в прямом порядке: ");
            for (int i = 0; i < arrayLength; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();

            Console.WriteLine("Массив в обратном порядке: ");
            for (int i = arrayLength; i > 0; i--)
            {
                Console.Write(array[i-1]+" ");
            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
