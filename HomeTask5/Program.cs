﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Рост каждого из 22 учеников представлена в виде массива.
// Определить количество учеников, рост которых не превышает r.

namespace HomeTask5
{
    class Program
    {
        static void Main(string[] args)
        {
            int j = 0;
            int amountStudents = 22;

            int[] Height = new int[amountStudents];

            Random rnd = new Random();

            for (int i = 0; i < amountStudents; i++)
            {
                Height[i] = rnd.Next(150, 200);
            }

            Console.Write("Все студенты: ");
            for (int i = 0; i < amountStudents; i++)
            {
                Console.Write(Height[i] + " ");
            }
            Console.WriteLine();

            Console.Write("Введите рост R: ");
            int maxHeight = int.Parse(Console.ReadLine());

            for (int i = 0; i < amountStudents; i++)
            {
                if(Height[i] <= maxHeight)
                {
                    j++;
                }
            }

            Console.Write($"количество учеников, рост которых не превышает {maxHeight}: {j}");
            Console.ReadKey();
        }
    }
}
