﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Дан массив вещественных чисел. Каждый элемент больше 10,
// заменить его квадратным корнем

namespace HomeTask3
{
    class Program
    {
        static void Main(string[] args)
        {
            int arrayLength;
            Console.Write("Введите длину массива: ");
            arrayLength = int.Parse(Console.ReadLine());

            double[] array = new double[arrayLength];

            Random rnd = new Random();

            for (int i = 0; i < arrayLength; i++)
            {
                array[i] = rnd.Next(100);
            }
            Console.WriteLine("Массив целиком: ");
            for (int i = 0; i < arrayLength; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();

            
            for (int i = 0; i < arrayLength; i++)
            {
                if (array[i] > 10.0)
                {
                    array[i] = Math.Sqrt(array[i]);
                }
            }

            Console.WriteLine("С учетом изменений: ");
            for (int i = 0; i < arrayLength; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
            Console.ReadKey();
            }
    }
}
