﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Дан массив. Найти сумму элементов массива, значения которых
// не более 20

namespace HomeTask4
{
    class Program
    {
        static void Main(string[] args)
        {
            int arrayLength;
            Console.Write("Введите длину массива: ");
            arrayLength = int.Parse(Console.ReadLine());

            int[] array = new int[arrayLength];

            Random rnd = new Random();

            for (int i = 0; i < arrayLength; i++)
            {
                array[i] = rnd.Next(100);
            }
            Console.WriteLine("Массив целиком: ");
            for (int i = 0; i < arrayLength; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();

            int sum = 0;

            for (int i = 0; i < arrayLength; i++)
            {
                if (array[i] > 20)
                {
                    sum += array[i];
                }
            }

            Console.WriteLine("Сумма элементов, больше 20: ");
            Console.WriteLine(sum);
            Console.ReadKey();
        }
    }
}
