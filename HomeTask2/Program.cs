﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Дан массив целых чисел. Найти номера элементов, оканчивающихся на 0.

namespace HomeTask2
{
    class Program
    {
        static void Main(string[] args)
        {
            int arrayLength;
            Console.Write("Введите длину массива: ");
            arrayLength = int.Parse(Console.ReadLine());

            int[] array = new int[arrayLength];

            Random rnd = new Random();

            for (int i = 0; i < arrayLength; i++)
            {
                array[i] = rnd.Next(-100, 100);
            }
            Console.WriteLine("Массив целиком: ");
            for (int i = 0; i < arrayLength; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
            bool j = false;
            Console.WriteLine("Числа, заканчивающиеся на 0: ");
            for (int i = 0; i < arrayLength; i++)
            {
                if (array[i] % 10 == 0)
                {
                    Console.Write(array[i] + " ");
                    j = true;
                }
            }
            if(j == false)
            {
                Console.WriteLine("Таких чисел нет.");
            }
            Console.ReadKey();
        }
    }
}
